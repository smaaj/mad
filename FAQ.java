package com.example.madproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class FAQ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        RecyclerView faqList = (RecyclerView) findViewById(R.id.faqList);
        faqList.setLayoutManager(new LinearLayoutManager(this));
        String[] faqdata = {"1. What is Zakat?", "Zakat is an important pillar among the five pillars of Islam.\n" +
                "It is an obligatory act ordained by Allah (The Glorified and Exalted) to be performed by every adult and able bodied Muslim.\n" +
                "It is levied on specific assets only, identified by Shariah (Islamic Law) as assets having the potential for growth.\n" +
                "It is a part of the wealth and property that Muslims must pay annually, to help the poor of their community.\n" +
                "It is levied at the rate of 2.5% each year (calculated according to the lunar calendar) on the market value of the Zakat-able assets after deducting there from specified liabilities.\n" +
                "The compulsory transfer of ownership of a portion of the property of the giver, calculated at the rate of 2.5% as aforesaid, to a poor and needy Muslim who qualifies to receive Zakat according to the Shariah.\n" +
                "It is an Ibada’h (worship) and not a tax, Obligatory on every Muslim who owns Nisab viz. 613.35 grams of silver, or 87.49 grams of gold or who owns one or more assets liable to Zakat, equal in value to 613.35 grams of silver or 87.49 grams of gold.",
                "2. What does Zakat mean?", "Zakat literally means “to be clear, to grow, to increase.” It comes from the root letters za, kaf, ya, which has several meanings: to be clean, to pay the obligatory charity, to be pure, innocent, to be better in purity, and to praise oneself, to justify. It has been used in the Quran to mean all of these things. It can also be considered a form of sadaqah (charity), given to the poor." ,
                "3. What is Nisba?", "The amount of zakatable wealth/asset which makes one liable for paying zakat is called Nisab. In other words any person who has Nisab in his possession for one lunar calendar has an obligation to pay Zakat. Similarly if a person does not have Nisab in his possession he/she can take Zakat."};
        faqList.setAdapter(new ProgrammingAdapter(faqdata));
    }
}
