package com.example.madproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ZakatCalculator extends AppCompatActivity
{
    private EditText bbET,hcET,gET,pET,apET;
    TextView Result;
    double zakatpercent=0.025;
    Button RButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_calculator);

        bbET = (EditText)findViewById(R.id.bbEt);
        hcET = (EditText)findViewById(R.id.hcET);
        gET = (EditText)findViewById(R.id.gET);
        pET = (EditText)findViewById(R.id.pET);
        apET = (EditText)findViewById(R.id.apET);
        Result =(TextView)findViewById(R.id.Result);
        RButton=(Button) findViewById(R.id.RButton);


        RButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Double num1 = Double.parseDouble(bbET.getText().toString());
                Double num2 = Double.parseDouble(hcET.getText().toString());
                Double num3 = Double.parseDouble(gET.getText().toString());
                Double num4 = Double.parseDouble(pET.getText().toString());
                Double num5 = Double.parseDouble(apET.getText().toString());

                Double value = num1 + num2 + num3 + num4 + num5;

                Double zakat = value * zakatpercent;

                Result.setText(Double.toString(zakat));
            }
        });
    }
}
